The following tools are required to be able to us the project:
1. GCC Compiler. For MAC install XCode command line tools
2. Make tool. See 1.
3. lcov. For MAC see instruction on http://macappstore.org/lcov/ ?

Once the tools are installed, the coverage report can be built using the command:
make lcov-report

This will build the example program, execute the program and generate the coverage report.
The coverage report is located in coverage_report/html/

In order to build an older version of the tool as comparison, use git to checkout the older version.
i.e. git checkout 54c0438c86f0ad769fc4acb05aca9ec0350222b8
The report can be rebuilt using:
make clean
make lcov-report

To return to the newest version of the repository execute:
git checkout master

## Specify the programs used in this script
CC = gcc

ECHO = echo
MKDIR = mkdir
RM = rm
SED = sed

GCOV = $(call dequote,$(CONFIG_TOOLPREFIX))gcov
LCOV = lcov
GENHTML = genhtml

REPORT_DIR = coverage_report

## Specify the compiler flags
CFLAGS = -Wall -Werror -g -O0 -fprofile-arcs -ftest-coverage -Iinc
LDFLAGS = -static -L./src

SUBDIRS = src


## Specify the target file
TARGET = gcov-test

## This variable holds all source files to consider for the build
SOURCES = main.c

## Specify additional libraries to link against
LIBS = -lcode -lgcov

# Object file definitions based on the source files
OBJS=$(join $(addsuffix ./obj/, $(dir $(SOURCES))), $(notdir $(SOURCES:.c=.o)))

# Dependency file definitions based on the source files
DEPENDS=$(join $(addsuffix ./dep/, $(dir $(SOURCES))), $(notdir $(SOURCES:.c=.d)))

GCDA=$(join $(addsuffix ./obj/, $(dir $(SOURCES))), $(notdir $(SOURCES:.c=.gcda)))
GCNO=$(join $(addsuffix ./obj/, $(dir $(SOURCES))), $(notdir $(SOURCES:.c=.gcno)))

BUILDDIRS = $(SUBDIRS:%=build-%)

CLEANDIRS = $(SUBDIRS:%=clean-%)

all: $(BUILDDIRS) $(TARGET)

.PHONY: subdirs $(SUBDIRS)
$(SUBDIRS):	$(BUILDDIRS)

.PHONY: subdirs $(BUILDDIRS)
$(BUILDDIRS):
	$(MAKE) -C $(@:build-%=%)

clean: $(CLEANDIRS)
	@-$(RM) -f $(TARGET) $(OBJS) $(DEPENDS) $(GCDA) $(GCNO)
	@$(ECHO) "Remove coverage data files..."
	@$(RM) -rf $(REPORT_DIR)

## Build the target from the objects
$(TARGET): $(OBJS)
	@$(ECHO) "============="
	@$(ECHO) "Linking the target $@"
	@$(CC) -o $@ $^ $(LDFLAGS) $(LIBS)

./obj/%.o : %.c
	@$(MKDIR) -p $(dir $@)
	@$(ECHO) "============="
	@$(ECHO) "Compiling $<"
	$(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@

## Make dependancy rules
./dep/%.d : %.c
	@$(MKDIR) -p $(dir $@)
	@$(ECHO) "============="
	@$(ECHO) Building dependencies file for $*.o
	@$(SHELL) -ec '$(CC) -M $(CFLAGS) $(INCLUDE) $< | $(SED) "s^$*.o^../obj/$*.o^" > $@'

.PHONY: subdirs $(CLEANDIRS)
$(CLEANDIRS):
	$(MAKE) -C $(@:clean-%=%) clean

lcov-report: $(BUILDDIRS) $(TARGET)
	@$(ECHO) "Generating coverage report in $(REPORT_DIR)"
	@$(ECHO) "Using gcov: $(GCOV)"
	@$(MKDIR) -p $(REPORT_DIR)/html
	@$(LCOV) --gcov-tool $(GCOV) --zerocounters --directory .
	@$(LCOV) --gcov-tool $(GCOV) --capture --initial --directory . --output-file $(REPORT_DIR)/gcov-test.info
	./$(TARGET)
	@$(LCOV) --gcov-tool $(GCOV) --capture --directory . --output-file $(REPORT_DIR)/gcov-test.info
	@$(GENHTML) --highlight --legend --output-directory $(REPORT_DIR)/html $(REPORT_DIR)/gcov-test.info

.PHONY: all clean lcov-report

## Include the dependency files
-include $(DEPENDS)

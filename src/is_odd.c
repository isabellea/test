
#include "is_odd.h"

bool is_odd_number(int number)
{
    bool odd_number;

    if (0 == (number % 2))
    {
        odd_number = false;
    }
    else
    {
        odd_number = true;
    }

    return odd_number;
}


bool is_even_number(int number)
{
    bool even_number;

    if (0 == (number % 2))
    {
        even_number = true;
    }
    else
    {
        even_number = false;
    }

    return even_number;
}


#include <stddef.h>
#include <stdio.h>

#include "calculator.h"

static double add(double first_number, double second_number);
static double subtract(double first_number, double second_number);
static double multiply(double first_number, double second_number);
static double divide(double first_number, double second_number);

int polish_notation_calculator(int first_number, int second_number, char operator, double * const result)
{
    int return_value = 0;

    if (NULL != result)
    {
        switch(operator)
        {
        case '+':
            *result = add(first_number, second_number);
            break;

        case '-':
            *result = subtract(first_number, second_number);
            break;

        case '*':
            *result = multiply(first_number, second_number);
            break;

        case '/':
            *result = divide(first_number, second_number);
            break;

        /* operwtor does not match +, -, * or / */
        default:
            fprintf(stderr, "ERROR: Unsupported operator %c \n", operator);
            return_value = -2;
        }
    }
    else
    {
        fprintf(stderr, "ERROR: result parameter has not been defined\n");
        return_value = -1;
    }

    return return_value;
}

static double add(double first_number, double second_number)
{
    double result;

    result = first_number + second_number;

    return result;
}

static double subtract(double first_number, double second_number)
{
    double result;

    result = first_number - second_number;

    return result;
}

static double multiply(double first_number, double second_number)
{
    double result;

    result = first_number * second_number;

    return result;
}

static double divide(double first_number, double second_number)
{
    double result;

    result = first_number / second_number;

    return result;
}

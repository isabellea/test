
#include "is_alphabet.h"

bool is_alphabet(char character)
{
    bool is_alphabet;

    if (
        (('a' <= character) && ('z' >= character)) ||
        (('A' <= character) && ('Z' >= character))
       )
    {
        is_alphabet = true;
    }        
    else
    {
        is_alphabet = false;
    }

    return is_alphabet;
}

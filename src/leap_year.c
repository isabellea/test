
#include "leap_year.h"

bool is_leap_year(int year)
{
    bool leap_year;

    if (0 == (year % 4))
    {
        if (0 == (year % 100))
        {
            // year is divisible by 400, hence the year is a leap year
            if (0 ==  (year % 400))
            {
                leap_year = true;
            }
            else
            {
                leap_year = false;
            }
        }
        else
        {
            leap_year = true;
        }
    }
    else
    {
        leap_year = false;
    }

    return leap_year;
}

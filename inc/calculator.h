#ifndef __CALCULATOR_H__
#define __CALCULATOR_H__

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

extern int polish_notation_calculator(int first_number, int second_number, char operator, double * const result);

#ifdef __cplusplus
}
#endif


#endif /* __CALCULATOR_H__ */

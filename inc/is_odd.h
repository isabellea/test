#ifndef __IS_ODD_H__
#define __IS_ODD_H__

#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

extern bool is_odd_number(int number);

extern bool is_even_number(int number);

#ifdef __cplusplus
}
#endif


#endif /* __IS_ODD_H__ */

#ifndef __IS_ALPHABET_H__
#define __IS_ALPHABET_H__

#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

extern bool is_alphabet(char character);

#ifdef __cplusplus
}
#endif


#endif /* __IS_ALPHABET_H__ */

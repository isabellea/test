#ifndef __LEAP_YEAR_H__
#define __LEAP_YEAR_H__

#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

extern bool is_leap_year(int year);

#ifdef __cplusplus
}
#endif


#endif /* __LEAP_YEAR_H__ */

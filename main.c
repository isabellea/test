
#include <stdio.h>

#include "calculator.h"
#include "is_alphabet.h"
#include "is_odd.h"
#include "leap_year.h"

int main(int argc, char** argv)
{
    int years[] = {1999, 2000, 2100, 168};
    bool is_true;
    double result;
    int i;

    for (i = 0; i < sizeof(years) / sizeof(int); ++i)
    {
        is_true = is_leap_year(years[i]);
        fprintf(stdout, "%4d is %sa leap year\n", years[i], false == is_true ? "NOT ": "");
    }

    if (false == is_odd_number(4))
    {
        is_even_number(4);
    }

    is_alphabet('*');
    is_alphabet('O');
    is_alphabet('j');

    polish_notation_calculator(10, 3.4, '/', &result);

    printf("10 3.4 * = %.1lf\n", result);

    return 0;
}
